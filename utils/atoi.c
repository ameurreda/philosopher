/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rameur <rameur@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/14 02:21:10 by rameur            #+#    #+#             */
/*   Updated: 2021/12/09 15:08:27 by rameur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int	ft_atoi(const char *str)
{
	int			i;
	int			neg;
	long long	res;

	i = 0;
	res = 0;
	neg = 1;
	while (str[i] == '\n' || str[i] == '\t'
		|| str[i] == '\v' || str[i] == '\f'
		|| str[i] == '\r' || str[i] == ' ')
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			neg = -1;
		i++;
	}
	while (str[i] && (str[i] >= 48 && str[i] <= 57))
	{
		res = (res * 10) + str[i] - '0';
		i++;
	}
	if (str[i] != '\0' && (str[i] < 48 || str[i] > 57))
		return (-1);
	return (res * neg);
}

void	ft_set_dead(t_struct *cfg, int i)
{
	pthread_mutex_unlock(&cfg->ph[i]);
	pthread_mutex_lock(&cfg->write);
	printf("%ld %d died\n", get_time() - cfg->start, i + 1);
	pthread_mutex_unlock(&cfg->write);
	cfg->is_dead = 1;
	pthread_mutex_unlock(&cfg->death);
}

int	ft_check_end(t_struct *cfg)
{
	pthread_mutex_lock(&cfg->ph[cfg->nb_philo - 1]);
	if (cfg->is_dead == 1 || cfg->philo[cfg->nb_philo - 1].count
		== cfg->nb_philo_eat)
	{
		pthread_mutex_unlock(&cfg->ph[cfg->nb_philo - 1]);
		return (1);
	}
	pthread_mutex_unlock(&cfg->ph[cfg->nb_philo - 1]);
	return (0);
}

int	ft_malloc_all(t_struct *cfg)
{
	cfg->philo = malloc(cfg->nb_philo * sizeof(t_str));
	if (cfg->philo == NULL)
		return (1);
	cfg->forks = malloc(cfg->nb_philo * sizeof(pthread_mutex_t));
	if (cfg->forks == NULL)
		return (1);
	cfg->ph = malloc(cfg->nb_philo * sizeof(pthread_mutex_t));
	if (cfg->ph == NULL)
		return (1);
	return (0);
}
