/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rameur <rameur@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/07 17:54:37 by rameur            #+#    #+#             */
/*   Updated: 2021/12/09 15:08:20 by rameur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <sys/time.h>
# include <pthread.h>
# include <unistd.h>

/*
**status ->
**1 = eating;
**2 = sleeping;
**3 = thinking;
*/
typedef struct s_str
{
	int			philo_id;
	long		last_meal;
	int			count;
	pthread_t	thread;
}				t_str;

typedef struct s_struct
{
	int				nb_philo;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				nb_philo_eat;
	int				is_dead;
	int				i;
	long			start;

	pthread_t		table;
	pthread_mutex_t	write;
	pthread_mutex_t	death;
	pthread_mutex_t	*forks;
	pthread_mutex_t	*ph;
	pthread_mutex_t	id;

	t_str			*philo;
}				t_struct;

int		ft_parse(int ac, char **av, t_struct *cfg);
int		ft_atoi(const char *str);
void	ft_philo(t_struct *cfg);
long	get_time(void);
void	ft_print(t_struct *cfg, long time, int id, char *str);
int		ft_check_stop(t_struct *cfg, int id);
int		ft_exit(t_struct *cfg, char *str, int exitCode);
void	ft_set_dead(t_struct *cfg, int i);
int		ft_check_end(t_struct *cfg);
int		ft_malloc_all(t_struct *cfg);

#endif