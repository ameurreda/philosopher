/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rameur <rameur@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/14 02:48:34 by rameur            #+#    #+#             */
/*   Updated: 2021/12/09 14:25:42 by rameur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void	ft_take_forks(t_struct *cfg, int id)
{
	pthread_mutex_lock(&cfg->forks[id]);
	ft_print(cfg, get_time(), id + 1, "has taken a fork\n");
	if (cfg->nb_philo == 1)
	{
		usleep(cfg->time_to_die * 1000);
		ft_print(cfg, get_time(), id + 1, "died\n");
		cfg->is_dead = 1;
		pthread_mutex_unlock(&cfg->forks[id]);
		return ;
	}
	if (id == cfg->nb_philo - 1)
	{
		pthread_mutex_lock(&cfg->forks[0]);
		ft_print(cfg, get_time(), id + 1, "has taken a fork\n");
	}
	else
	{
		pthread_mutex_lock(&cfg->forks[id + 1]);
		ft_print(cfg, get_time(), id + 1, "has taken a fork\n");
	}
}

void	ft_eat(t_struct *cfg, int id)
{
	ft_take_forks(cfg, id);
	ft_print(cfg, get_time(), id + 1, "is eating\n");
	pthread_mutex_lock(&cfg->ph[id]);
	cfg->philo[id].last_meal = get_time();
	cfg->philo[id].count++;
	pthread_mutex_unlock(&cfg->ph[id]);
	usleep(cfg->time_to_eat * 1000);
	pthread_mutex_unlock(&cfg->forks[id]);
	if (id == cfg->nb_philo - 1)
		pthread_mutex_unlock(&cfg->forks[0]);
	else
		pthread_mutex_unlock(&cfg->forks[id + 1]);
}

void	ft_sleep(t_struct *cfg, int id)
{
	ft_print(cfg, get_time(), id + 1, "is sleeping\n");
	usleep(cfg->time_to_sleep * 1000);
	ft_print(cfg, get_time(), id + 1, "is thinking\n");
}

void	*ft_tasks(void *arg)
{
	t_struct	*cfg;
	int			id;

	cfg = (t_struct *)arg;
	pthread_mutex_lock(&cfg->id);
	id = cfg->i;
	cfg->i++;
	pthread_mutex_unlock(&cfg->id);
	if (id % 2 != 0)
		usleep(cfg->time_to_eat * 1000);
	while (1)
	{
		ft_eat(cfg, id);
		if (ft_check_stop(cfg, id) == 1)
			return (0);
		ft_sleep(cfg, id);
		if (ft_check_stop(cfg, id) == 1)
			return (0);
	}
}

void	ft_philo(t_struct *cfg)
{
	int	i;

	cfg->i = 0;
	i = 0;
	while (i < cfg->nb_philo)
	{
		pthread_create(&(cfg->philo[i].thread), NULL, &ft_tasks, (void *)cfg);
		i++;
	}
	i = 0;
	while (i < cfg->nb_philo)
	{
		pthread_join(cfg->philo[i].thread, 0);
		i++;
	}
}
