/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rameur <rameur@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/14 02:17:35 by rameur            #+#    #+#             */
/*   Updated: 2021/12/09 15:07:49 by rameur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int	ft_init(t_struct *cfg)
{	
	cfg->nb_philo = -1;
	cfg->time_to_die = -1;
	cfg->time_to_eat = -1;
	cfg->time_to_sleep = -1;
	cfg->nb_philo_eat = -1;
	cfg->is_dead = 0;
	cfg->philo = NULL;
	if (pthread_mutex_init(&cfg->death, 0) != 0)
		return (1);
	if (pthread_mutex_init(&cfg->write, 0) != 0)
		return (1);
	if (pthread_mutex_init(&cfg->id, 0) != 0)
		return (1);
	return (0);
}

int	ft_check(t_struct *cfg, int ac)
{
	if (cfg->nb_philo < 1 || cfg->time_to_die <= 0 || cfg->time_to_eat <= 0
		|| cfg->time_to_sleep <= 0)
		return (1);
	else if (ac == 6 && cfg->nb_philo_eat <= 0)
		return (1);
	return (0);
}

int	ft_check_stop(t_struct *cfg, int id)
{
	pthread_mutex_lock(&cfg->death);
	if (cfg->is_dead == 1)
	{
		pthread_mutex_unlock(&cfg->death);
		return (1);
	}
	pthread_mutex_unlock(&cfg->death);
	if (cfg->nb_philo_eat != -1 && cfg->philo[id].count == cfg->nb_philo_eat)
		return (1);
	return (0);
}

int	ft_get_arg(t_struct *cfg, char **av)
{
	cfg->nb_philo = ft_atoi(av[1]);
	cfg->time_to_die = ft_atoi(av[2]);
	cfg->time_to_eat = ft_atoi(av[3]);
	cfg->time_to_sleep = ft_atoi(av[4]);
	if (cfg->nb_philo < 1 || cfg->nb_philo > 200)
		return (1);
	return (0);
}

int	ft_parse(int ac, char **av, t_struct *cfg)
{
	int	i;

	i = 0;
	if (ft_init(cfg) == 1)
		return (2);
	if (ft_get_arg(cfg, av) == 1)
		return (2);
	if (ft_malloc_all(cfg) == 1)
		return (1);
	while (i < cfg->nb_philo)
	{
		cfg->philo[i].philo_id = i + 1;
		cfg->philo[i].last_meal = get_time();
		cfg->philo[i].count = 0;
		pthread_mutex_init(&cfg->ph[i], 0);
		pthread_mutex_init(&cfg->forks[i], 0);
		i++;
	}
	if (ac == 6)
		cfg->nb_philo_eat = ft_atoi(av[5]);
	if (ft_check(cfg, ac) == 1)
		return (1);
	return (0);
}
