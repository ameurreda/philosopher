SRCS            = main.c\
					srcs/parse.c\
					srcs/philo.c\
					utils/atoi.c\


CC              = clang
LIB				= ./includes/main.h
CFLAGS          = -g -Wall -Wextra -Werror -I $(dir $(LIB))
NAME            = philo

OBJS            = ${SRCS:.c=.o}

all:            $(NAME)

.c.o:
		${CC} ${CFLAGS} -c $< -o ${<:.c=.o}

$(NAME):	$(OBJS) $(LIB)
					${CC} ${CFLAGS} ${OBJS} -o ${NAME} -lpthread

clean:
		rm -f utils/*.o
		rm -f srcs/*.o
		rm -f *.o

fclean:		clean
				${RM} ${NAME}

re:             clean all

.PHONY: all clean fclean re