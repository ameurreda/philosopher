/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rameur <rameur@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/07 17:53:58 by rameur            #+#    #+#             */
/*   Updated: 2021/12/09 15:03:49 by rameur           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

int	ft_exit(t_struct *cfg, char *str, int exitCode)
{
	if (exitCode != 3)
		printf("%s\n", str);
	if (exitCode == 1)
	{
		free(cfg->forks);
		free(cfg->philo);
		free(cfg->ph);
	}
	else if (exitCode == 3)
	{
		pthread_mutex_destroy(&cfg->write);
		pthread_mutex_destroy(&cfg->death);
		pthread_mutex_destroy(&cfg->id);
		pthread_mutex_destroy(cfg->forks);
		pthread_mutex_destroy(cfg->ph);
		free(cfg->forks);
		free(cfg->ph);
		free(cfg->philo);
	}
	return (exitCode);
}

long	get_time(void)
{
	struct timeval	time;
	long			res;

	gettimeofday(&time, NULL);
	res = (time.tv_sec * 1000) + (time.tv_usec / 1000);
	return (res);
}

void	ft_print(t_struct *cfg, long time, int id, char *str)
{
	long	res;

	res = time - cfg->start;
	pthread_mutex_lock(&(cfg->write));
	pthread_mutex_lock(&cfg->death);
	if (cfg->is_dead == 0)
		printf("%ld %d %s", res, id, str);
	pthread_mutex_unlock(&cfg->death);
	pthread_mutex_unlock(&(cfg->write));
}

void	*ft_table(void *arg)
{
	int			i;
	t_struct	*cfg;

	i = -1;
	cfg = (t_struct *)arg;
	while (++i <= cfg->nb_philo)
	{
		if (ft_check_end(cfg) == 1)
			return (0);
		pthread_mutex_lock(&cfg->death);
		pthread_mutex_lock(&cfg->ph[i]);
		if (cfg->is_dead == 0 && get_time() > cfg->philo[i].last_meal
			+ cfg->time_to_die)
		{
			ft_set_dead(cfg, i);
			return (0);
		}
		pthread_mutex_unlock(&cfg->ph[i]);
		pthread_mutex_unlock(&cfg->death);
		if (i == cfg->nb_philo - 1)
			i = -1;
		usleep(100);
	}
	return (NULL);
}

int	main(int ac, char **av)
{
	t_struct	cfg;
	int			i;

	if (ac >= 5 && ac <= 6)
	{
		i = ft_parse(ac, av, &cfg);
		if (i == 1)
			return (ft_exit(&cfg, "Error: parse invalid", 1));
		else if (i == 2)
			return (ft_exit(&cfg, "Error: parse invalid", 2));
		cfg.start = get_time();
		pthread_create(&(cfg.table), NULL, &ft_table, (void *)(&cfg));
		ft_philo(&cfg);
		pthread_join(cfg.table, 0);
		ft_exit(&cfg, "", 3);
	}
	else
		printf("Error: arguments\n");
	return (0);
}
